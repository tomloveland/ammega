# Taken from https://www.tenforums.com/tutorials/23504-restore-default-location-personal-folders-windows-10-a.html

# Redirects the Documents, Videos, Pictures, and Music folders back to their default locations.

# Variables
$defaultDocumentsPath = "$env:USERPROFILE\Documents"
$defaultVideosPath = "$env:USERPROFILE\Videos"
$defaultPicturesPath = "$env:USERPROFILE\Pictures"
$defaultMusicPath = "$env:USERPROFILE\Music"
$shellFoldersPath = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
$userShellFoldersPath = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders"

# Have to kill explorer to make the change
Stop-Process -Name explorer -Force -Confirm:$false

# Changing Documents
if((Test-Path -Path $defaultDocumentsPath) -eq $false)
{
    New-Item -Path $defaultDocumentsPath -ItemType Directory -Force -Confirm:$false
    attrib +r -s -h $defaultDocumentsPath /S /D

}
New-ItemProperty -Path $shellFoldersPath -Name "Personal" -Value $defaultDocumentsPath -PropertyType String -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "{f42ee2d3-909f-4907-8871-4c22fc0bf756}" -Value $defaultDocumentsPath -PropertyType ExpandString -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "Personal" -Value $defaultDocumentsPath -PropertyType ExpandString -Force -Confirm:$false

# Changing Videos
if((Test-Path -Path $defaultVideosPath) -eq $false)
{
    New-Item -Path $defaultVideosPath -ItemType Directory -Force -Confirm:$false
    attrib +r -s -h $defaultVideosPath /S /D

}
New-ItemProperty -Path $shellFoldersPath -Name "My Video" -Value $defaultVideosPath -PropertyType String -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "{35286A68-3C57-41A1-BBB1-0EAE73D76C95}" -Value $defaultVideosPath -PropertyType ExpandString -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "My Video" -Value $defaultVideosPath -PropertyType ExpandString -Force -Confirm:$false

# Changing Pictures
if((Test-Path -Path $defaultPicturesPath) -eq $false)
{
    New-Item -Path $defaultPicturesPath -ItemType Directory -Force -Confirm:$false
    attrib +r -s -h $defaultPicturesPath /S /D

}
New-ItemProperty -Path $shellFoldersPath -Name "My Pictures" -Value $defaultPicturesPath -PropertyType String -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "{0DDD015D-B06C-45D5-8C4C-F59713854639}" -Value $defaultPicturesPath -PropertyType ExpandString -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "My Pictures" -Value $defaultPicturesPath -PropertyType ExpandString -Force -Confirm:$false

# Changing Music
if((Test-Path -Path $defaultMusicPath) -eq $false)
{
    New-Item -Path $defaultMusicPath -ItemType Directory -Force -Confirm:$false
    attrib +r -s -h $defaultMusicPath /S /D

}
New-ItemProperty -Path $shellFoldersPath -Name "My Music" -Value $defaultMusicPath -PropertyType String -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "{A0C69A99-21C8-4671-8703-7934162FCF1D}" -Value $defaultMusicPath -PropertyType ExpandString -Force -Confirm:$false
New-ItemProperty -Path $userShellFoldersPath -Name "My Music" -Value $defaultMusicPath -PropertyType ExpandString -Force -Confirm:$false

# Starting explorer again
Start-Process -FilePath "C:\Windows\explorer.exe"

# But then stopping it again because for some reason it needs to be stopped twice.
Stop-Process -Name explorer -Force -Confirm:$false

# Starting explorer for the final time
Start-Process -FilePath "C:\Windows\explorer.exe"
