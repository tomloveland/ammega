[array]$inPaths = "E:\General",
                  "E:\TN",
                  "E:\Trainers",
                  "F:\Company_Documents",
                  "F:\DeprData",
                  "F:\eTime-ADP",
                  "F:\GSS-Scans",
                  "F:\Locations",
                  "F:\Managers",
                  "F:\Sales Budget"         

[string]$outPath = "C:\$env:HOMEPATH\Desktop\output.csv"
if ((Test-Path -Path $outPath) -eq $true)
{
    Remove-Item -Path $outPath -Force -Confirm:$false
}

foreach ($inPath in $inPaths)
{
    $filesInfos = Get-ChildItem -Path $inPath -Recurse

    foreach ($fileInfo in $filesInfos)
    {
        $fullName = $fileInfo.FullName
        if(($fullName | Select-String -Pattern "F:\Locations\Appleton\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Atlanta\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Branch 50\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Chicago\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Corporate\users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Denver\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Florida\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Ft. Smith\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Inventory\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Kansas City\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Memphis\Users" -SimpleMatch) -or ($fullName | Select-String -Pattern "F:\Locations\Nashville\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\Nashville\Users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\st louis\users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\utah\users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\texas\users" -SimpleMatch -Quiet) -or ($fullName | Select-String -Pattern "F:\Locations\ohio\users" -SimpleMatch -Quiet))
        {}
        else
        {
            if ($fileInfo.PSIsContainer -eq $true)
            {
                [bool]$isDirectory = $true
            }
            else
            {
                [bool]$isDirectory = $false
            }

            $levelsDeep = ($fullName.ToCharArray() | Where-Object { $_ -eq '\' } | Measure-Object).Count

            $row = "" | Select-Object -Property FullPath, Name, ParentDirectory, IsDirectory, Extension, CreationTime, LastWriteTime, LastAccessTime, LevelsDeep, LengthInKB
            $row.FullPath = $fullName
            $row.Name = $fileInfo.Name
            $row.ParentDirectory = $fileInfo.Directory
            $row.IsDirectory = $isDirectory
            $row.Extension = $fileInfo.Extension
            $row.CreationTime = $fileInfo.CreationTime
            $row.LastWriteTime = $fileInfo.LastWriteTime
            $row.LastAccessTime = $fileInfo.LastAccessTime
            $row.LevelsDeep = $levelsDeep
            $row.LengthInKB = [math]::Round($fileInfo.Length / 1KB, 2)
            $row | Export-Csv -Path $outPath -NoTypeInformation -Force -Confirm:$false -Append        
        }     
    }
}
